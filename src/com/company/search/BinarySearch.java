package com.company.search;

import com.company.sort.RadixSort;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class BinarySearch {
    private final String outputFile;
    private final String inputSearchFile;
    private final int number;
    private int[] arr;
    private int[] arrInputSearch;
    private ArrayList<Integer> result= new ArrayList<>();

    public BinarySearch(String outputFile, String inputSearchFile, int number) {
        this.number = number;
        this.outputFile = outputFile;
        this.inputSearchFile = inputSearchFile;
    }

    public void search(int number) throws IOException {
        readOutputFile();
        readInputSearchFile();
        int j = 0;
        int i = 0;
        for (int k = 0; k < arrInputSearch.length; k++) {
            for (; i < number; i++) {
                if (arr[i] == arrInputSearch[k]) {
                    result.add(arr[i]) ;
                    if(i != number -1){
                        if (arr[i + 1] == arrInputSearch[k]) continue;
                        else break;
                    }
                }
            }
        }
        System.out.println(result.toString());
    }


    private void readOutputFile() throws IOException {
        FileReader fr = new FileReader(outputFile);
        BufferedReader reader = new BufferedReader(fr);
        String line = "";
        arr = new int[number];
        int i = 0;
        while ((line = reader.readLine()) != null) {
            String[] data = line.split(" ");
            for (String str : data) {
                try {
                    arr[i++] = Integer.parseInt(str);
                } catch (Exception ex) {
                    i--;
                }
            }
        }
    }

    private void readInputSearchFile() throws IOException {
        FileReader fr = new FileReader(inputSearchFile);
        BufferedReader reader = new BufferedReader(fr);
        String line = "";
        arrInputSearch = new int[20];
        int i = 0;
        while ((line = reader.readLine()) != null) {
            String[] data = line.split(" ");
            for (String str : data) {
                try {
                    arrInputSearch[i++] = Integer.parseInt(str);
                } catch (Exception ex) {
                    i--;
                }
            }
        }

        RadixSort radixSort = new RadixSort(arrInputSearch, arrInputSearch.length);
        arrInputSearch = radixSort.sort();
    }
}
