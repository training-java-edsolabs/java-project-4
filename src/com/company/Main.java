package com.company;

import com.company.search.BinarySearch;
import com.company.sort.SortFile;

import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;

public class Main {

    public static void main(String[] args) throws IOException, ExecutionException, InterruptedException {
        int number;
        String direction = null;
        int numberThread;
        long startTime;
        long finishTime;
        String outputFile = "src/com/company/file/output.txt";
        String inputFile = "src/com/company/file/input.txt";
        String inputSearch = "src/com/company/file/inputSearch.txt";
        String outputSearch = "src/com/company/file/outputSearch.txt";

        Scanner scanner = new Scanner(System.in);
        System.out.print("Nhập số phần tử: ");
        number = Integer.parseInt(scanner.nextLine());

        //Generate input number
        FileIO writeFile = new FileIO().writeFile(number);

        System.out.println("Số luồng:");
        numberThread = Integer.parseInt(scanner.nextLine());

        startTime = System.currentTimeMillis();
        SortFile sortFile =  SortFile.sortNumber(inputFile, numberThread, number);

        finishTime = System.currentTimeMillis();
        long timeSpend = finishTime - startTime;
        System.out.println("TimeSort:" + timeSpend);

        //Generate input number search
        FileIO inputSearchFile = new FileIO().inputSearchFile(number);

        //Search
        BinarySearch binarySearch= new BinarySearch(outputFile,inputSearch,number);
        binarySearch.search(number);

    }
}
