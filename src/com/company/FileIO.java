package com.company;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class FileIO {
    public static Random rand = new Random();
    int number;
    String outputFile = "src/com/company/file/output.txt";
    String inputFile = "src/com/company/file/input.txt";
    String inputSearchFile = "src/com/company/file/inputSearch.txt";
    String outputSearchFile = "src/com/company/file/outputSearch.txt";

    public FileIO writeFile(int number) {
        File file = new File(inputFile);
        try {
            FileWriter myWriter = new FileWriter(inputFile);
            for (int i = 0; i < number; i++) {
                myWriter.write(String.valueOf(rand.nextInt(number)));
                if (i % 20 == 0 && i != 0) {
                    myWriter.write("\n");
                } else {
                    myWriter.write(" ");
                }

            }
            myWriter.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return null;
    }

    public FileIO readFile(int length) {
        File file = new File(outputFile);
        try {
            FileWriter writer = new FileWriter(outputFile);
            for (int j = 0; j < length; j++) {
                writer.write(String.valueOf(j));
                if (j % 20 == 0 && j != 0) {
                    writer.write("\n");
                } else {
                    writer.write(" ");
                }
            }
            writer.close();

        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return null;
    }

    public FileIO inputSearchFile(int number){
        File file = new File(inputSearchFile);
        try {
            FileWriter myWriter = new FileWriter(inputSearchFile);
            for (int i = 0; i < 20; i++) {
                myWriter.write(String.valueOf(rand.nextInt(number)));
                myWriter.write(" ");
            }
            myWriter.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return null;
    }

    public FileIO outputSearchFile(int length) {
        File file = new File(outputSearchFile);
        try {
            FileWriter writer = new FileWriter(outputSearchFile);
            for (int j = 0; j < length; j++) {
                writer.write(String.valueOf(j));
                if (j % 20 == 0 && j != 0) {
                    writer.write("\n");
                } else {
                    writer.write(" ");
                }
            }
            writer.close();

        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return null;
    }

}
