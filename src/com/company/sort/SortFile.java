package com.company.sort;

import com.company.FileIO;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class SortFile {

    public static SortFile sortNumber(String direction, int numberThread, int number) throws IOException, ExecutionException, InterruptedException {
        FileReader fr = new FileReader(direction);
        BufferedReader reader = new BufferedReader(fr);
        String line = "";
        int[] arr = new int[number];
        int i = 0;
        while ((line = reader.readLine()) != null) {
            String[] data = line.split(" ");
            for (String str : data) {
                try {
                    arr[i++] = Integer.parseInt(str);
                } catch (Exception ex) {
                    i--;
                }
            }
        }

        //chỗ xử lý multithreading
        Map<Integer, int[]> mapList = new HashMap<>();
        for (int j = 0; j < numberThread; j++) {
            int[] half = Arrays.copyOfRange(arr, (arr.length / numberThread) * j, (arr.length / numberThread) * (j + 1));
            mapList.put(j, half);
        }

        ExecutorService executor = Executors.newFixedThreadPool(numberThread);
        List<Future> tasks = new ArrayList<>();

        for (int j = 0; j < numberThread; j++) {
            Future<int[]> task = executor.submit(new RequestHandler(mapList.get(j), "Thread " + (j + 1)));
            tasks.add(task);
        }

        for (int j = 0; j < numberThread; j++) {
            tasks.get(j).get();
        }

        // gộp các mảng sorted
        int[][] arr2d = tasks.stream()
                .map(l -> {
                    try {
                        return (int[]) l.get();
                    } catch (InterruptedException | ExecutionException e) {
                        e.printStackTrace();
                        return null;
                    }
                }).toArray(int[][]::new);
        int[] result = mergeSortedArray(arr2d);

        int length = result.length;

        //ghi file sorted
        FileIO readFile = new FileIO().readFile(length);

        return null;
    }

    public static int[] mergeSortedArray(int[][] arr) {
        PriorityQueue<ArrayBucket> queue = new
                PriorityQueue<ArrayBucket>();
        int total = 0;
        for (int[] ints : arr) {
            queue.add(new ArrayBucket(ints, 0));
            total = total + ints.length;
        }
        int m = 0;
        int[] result = new int[total];
        while (!queue.isEmpty()) {
            ArrayBucket ac = queue.poll();
            result[m++] = ac.arr[ac.index];
            if (ac.index < ac.arr.length - 1) {
                queue.add(new ArrayBucket(ac.arr, ac.index + 1));
            }
        }
        return result;
    }

}
