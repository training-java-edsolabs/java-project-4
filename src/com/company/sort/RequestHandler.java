package com.company.sort;

import java.util.concurrent.Callable;

public class RequestHandler implements Callable<int[]> {
    private int[] records;

    private String name;

    public RequestHandler(int[] records, String name) {
        this.records = records;
        this.name = name;
    }

    @Override
    public int[] call() throws Exception {
        RadixSort radixSort = new RadixSort(records, records.length);
        return radixSort.sort();
    }
}
